package com.easysolutions.interfacesandinheritance;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.easysolutions.interfacesandinheritance.hierarchyAnimal.AnimalPetCat;
import com.easysolutions.interfacesandinheritance.hierarchyMan.Man;

public class MainActivity extends AppCompatActivity {

    Button btnMan;
    Button btnAnimal;
    public static TextView tvCommonAnimal;
    public static TextView tvCommonMan;
    public static TextView tvManFriendly;
    public static TextView tvAnimalFriendly;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnMan = (Button)findViewById(R.id.btnMan);
        btnAnimal = (Button)findViewById(R.id.btnAnimal);
        tvCommonMan = (TextView)findViewById(R.id.tvCommonMan);
        tvCommonAnimal = (TextView)findViewById(R.id.tvCommonAnimal);
        tvManFriendly = (TextView)findViewById(R.id.tvManFriendly);
        tvAnimalFriendly = (TextView)findViewById(R.id.tvAnimalFriendly);

        btnMan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Frendly(new Man());
            }
        });

        btnAnimal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // Хоть класс AnimalPetCat пустой,
                // но он наследник от AnimalPet, в котором есть нужный метод.
                // При создании объекта класса Frendly запускается код в его конструкторе.
                new Frendly(new AnimalPetCat());
            }
        });
    }

    public void setManText (String string) {
        tvManFriendly.setText(string);
    }

    public void setAnimalText (String string) {
        tvAnimalFriendly.setText(string);
    }

    public void setCommonText (String string) {
        tvCommonMan.setText(string);
        tvCommonAnimal.setText(string);
    }

}
