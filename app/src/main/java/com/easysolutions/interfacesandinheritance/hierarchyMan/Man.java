package com.easysolutions.interfacesandinheritance.hierarchyMan;

import com.easysolutions.interfacesandinheritance.Frendly;
import com.easysolutions.interfacesandinheritance.MainActivity;

public class Man implements Frendly.InterfaceOfFriendly {

    @Override
    public void behavior(MainActivity mainActivity) {

        //mainActivity.setManText("Дружелюбный человек улыбается.");}

        if (mainActivity.tvManFriendly.getText().toString().equals("")) {
            mainActivity.setManText("Дружелюбный человек улыбается.");}
        else {
            mainActivity.setManText("");}
    }
}
