package com.easysolutions.interfacesandinheritance;

public class Frendly {

    public Frendly (InterfaceOfFriendly interfaceOfFriendly) {

        MainActivity mainActivity = new MainActivity();

        // 1) тут постоянная общая часть кода для всех пользователей дружелюбия
        mainActivity.setCommonText ("Сработала общая часть Friendly!\nИ люди и животные если дружелюбны, то они приветливы, испытывают радость и желание общаться.");

        // 2) тут запускается код, который переопределяется в зависимости от того, кто дружелюбен
        interfaceOfFriendly.behavior(mainActivity);

    }

    public interface InterfaceOfFriendly {
        void behavior (MainActivity mainActivity);
    }
}
