package com.easysolutions.interfacesandinheritance.hierarchyAnimal;

public class AnimalBlockExecutable extends AnimalPet {

    // таким образом запускается блок кода
    public AnimalBlockExecutable(Runnable r){
        r.run();
    }
}
