package com.easysolutions.interfacesandinheritance.hierarchyAnimal;

import com.easysolutions.interfacesandinheritance.Frendly;
import com.easysolutions.interfacesandinheritance.MainActivity;

public class AnimalPet extends Animal implements Frendly.InterfaceOfFriendly
{
    @Override
    public void behavior(MainActivity mainActivity) {

        //mainActivity.setAnimalText("Дружелюбное домашнее животное радостно крутится возле человека.");

        if (mainActivity.tvAnimalFriendly.getText().toString().equals("")) {
            mainActivity.setAnimalText("Дружелюбное домашнее животное радостно крутится возле человека.");
        }
        else {
            mainActivity.setAnimalText("");
        }
    }
}
